
"use strict";

const regex = function* (re, input) {
  const regexp = new RegExp(re);
  regexp.lastIndex = 0;

  let res;
  while ( (res = regexp.exec(input)) !== null ) {
    const [matched, ...substrings] = res;
    yield {matched: matched, groups: substrings};
  }
};

module.exports = regex;
