"use strict";

const random = (opts) => {
  if ( Array.isArray(opts) ) {
    const index = (Math.random() * opts.length) | 0;
    return opts[index];
  }

  const {
    min = 0.0,
    max = 1.0,
    int = false
  } = opts || {};

  const val = (Math.random() * (max - min)) + min;
  return (int && (val | 0)) || val;
};

module.exports = random;
