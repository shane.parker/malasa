"use strict";

const settle = promises =>
  Promise.all(promises.map(promise => promise.then(
    value => ({ value, status: true }),
    reason => ({ reason, status: false }))));

module.exports = settle;
