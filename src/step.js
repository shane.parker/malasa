"use strict";

const step = function* (collection) {
  let x = 0;
  for ( const v of collection ) {
    yield {value: v, index: x++};
  }
};

module.exports = step;
