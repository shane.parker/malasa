"use strict";

const except = require("./except");

const protocols = {
  "http:": {module: require("http"), port: 80},
  "https:": {module: require("https"), port: 443}
};

const parsers = {
  "application/x-www-form-urlencoded": v =>
    v.split("&").map( kv => kv.split("=") )
    .reduce( (acc, [key, val]) => {
        acc[decodeURIComponent(key)] = decodeURIComponent(val);
        return acc;
    }, {}),

  "application/json": (v) => JSON.parse(v)
};

const encoders = {
  "application/x-www-form-urlencoded": v => Object.entries(v).map( ([key, value]) =>
    encodeURIComponent(key) + "=" + encodeURIComponent(value) ).join("&"),
  "application/json": v => JSON.stringify(v),
  "text/plain": v => ("" + v)
};

const parseData = (type, data) => {
  const [useType,] = (type && type.split(";")) || [];
  const parser = parsers[useType];

  return (parser && parser(data)) || data;
};

const encodeData = (type, data) => {
  const [useType,] = (type && type.split(";")) || undefined;
  const encoder = encoders[useType];

  return (encoder && encoder(data)) || data;
};

const contentType = data => ((typeof data === 'object') && "application/x-www-form-urlencoded; charset=utf-8") ||
  "text/plain; charset=utf-8";

const queryString = query => Object.entries(query || {})
  .map( ([k, v]) => encodeURIComponent(k) + "=" + encodeURIComponent(v) )
  .join("&");

const requestBrowser = (url, opts) => {
  const uri = ((url instanceof URL) && url) || new URL(url);
  const options = opts || {};

  const {
    path = uri.pathname,
    qs = queryString(options.q),
    headers = {},
    method = options.method || ((options.body || options.json) && "POST") || "GET",
    parse = true,
    encoding = "utf8",
    timeout = 60000,
    body = options.json
  } = options;

  const type = headers["Content-Type"] ||
    (options.json && (headers["Content-Type"] = "application/json")) ||
    (headers["Content-Type"] = contentType(body));

  return new Promise( (resolve, reject) => {
    const req = new XMLHttpRequest();
    req.withCredentials = options.creds || false;
    req.open(method, `${uri.origin}${path}${(qs && ("?" + qs)) || uri.search}`);
    req.addEventListener("load", () => resolve({
      data: (parse && parseData(req.getResponseHeader("Content-Type"), req.responseText)) || req.responseText,
      get headers() {
        return (req.getAllResponseHeaders() || "").split(/[\r\n]+/).reduce( (acc, v) => {
          const [header, value] = v.split(": ");
          return (header && value) ? ((acc[header] = value) && acc) : acc;
        }, {});
      },
      status: req.status
    }));
    req.addEventListener("error", err => reject(err));

    Object.entries(headers).forEach(([key, value]) => req.setRequestHeader(key, value));
    req.send((body && encodeData(type, body)) || undefined);
  });
};

const requestNode = (url, opts) => {
  const uri = ((url instanceof URL) && url) || new URL(url);
  const protocol = protocols[uri.protocol] || except("Invalid protocol");
  const options = opts || {};

  const {
    path = uri.pathname,
    qs = queryString(options.q),
    headers = {},
    method = options.method || ((options.body || options.json) && "POST") || "GET",
    port = uri.port || protocol.port,
    parse = true,
    encoding = "utf8",
    timeout = 60000,
    body = options.json
  } = options;

  const type = headers["Content-Type"] ||
    (options.json && (headers["Content-Type"] = "application/json")) ||
    (headers["Content-Type"] = contentType(body));

  return new Promise( (resolve, reject) => {
    const chunks = [];

    const req = protocol.module.request({hostname: uri.hostname,
      path: `${path}${(qs && ("?" + qs)) || uri.search}`,
      port: port,
      headers: headers,
      method: method, timeout: timeout}, res => {
      res.setEncoding(encoding);
      res.on("data", data => chunks.push(data));
      res.on("end", () => {
        const combined = chunks.join("");
        return resolve({
          data: (parse && parseData(res.headers["content-type"], combined)) || combined,
          status: res.statusCode
        });
      });
    });

    req.on("error", err => reject(err));

    if ( body ) {
      req.write(encodeData(type, body));
    }

    req.end();
  });
};

module.exports = ((typeof window !== 'undefined' && window.XMLHttpRequest) && requestBrowser) || requestNode;
