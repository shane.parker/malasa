"use strict";

const ERROR = Symbol("Log Level: 3 (ERROR)");
const WARNING = Symbol("Log Level: 2 (WARNING)");
const INFO = Symbol("Log Level: 1 (INFO)");
const DEBUG = Symbol("Log Level: 0 (DEBUG)");

const levels = {[ERROR]: 3, [WARNING]: 2, [INFO]: 1, [DEBUG]: 0};
const levelNameMap = {ERROR, WARNING, INFO, DEBUG};

const isLevel = v => (v === ERROR) || (v === WARNING) || (v === INFO) || (v === DEBUG);
const currentLevel = levels[levelNameMap[(process.env.LOG_LEVEL || "").toUpperCase()] || ERROR];

const log = (...params) => {
  const plevel = isLevel(params[0]);
  const level = (plevel && params[0]) || INFO;
  if ( levels[level] >= currentLevel ) {
    console.log( ...(plevel ? params.splice(1) : params) );
  }

  return params;
};

Object.entries(levelNameMap).forEach( ([k, v]) => log[k] = v );
module.exports = log;
