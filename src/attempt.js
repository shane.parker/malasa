"use strict";

const sleep = require("./sleep");
const range = require("./range");

async function attempt(f, options) {
  const {maxTries = Number.POSITIVE_INFINITY, timeout = 1000, throttle = 0} = (options || {});

  for ( const x of range(0, maxTries) ) {
    const tmp = await f(x);
    if ( tmp !== undefined ) {
      return tmp;
    }

    await sleep(timeout + (x * throttle));
  }

  return undefined;
}

module.exports = attempt;
