"use strict";

const except = require("./except");

const contracts = {};

const contract = (className, ...methods) => {
  const clazz = (contracts[className] !== undefined) ? except(`Class/contract name already in use: ${className}`)
    : (contracts[className] = function () {});

  return methods.reduce( (cl, m) => {
    const name = (typeof m === 'string') ? m : m.get || m.set || except(`Invalid contract method: ${m}`);

    return Object.defineProperty(cl.prototype, name,
      m.get ? {get: function () { except(`Required getter not implemented on class ${this.constructor.name}: get ${name}()`); }}
      : m.set ? {set:  function () { except(`Required setter not implemented on class ${this.constructor.name}: set ${name}(value)`); }}
      : {value: function () { except(`Required method not implemented on class ${this.constructor.name}: ${name}`); }} ) && cl;
  }, clazz);
};

module.exports = contract;
