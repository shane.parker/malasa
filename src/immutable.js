"use strict";

const except = require("./except");

const throwSet = (target, property, value, receiver) => except(`setting key "${property}" on read only object`);
const nothrowSet = (target, property, value, receiver) => true;

const immutable = (data, opts) => new Proxy(data, {
  get: (target, property) => {
    const val = target.hasOwnProperty(property) && target[property];
    return ((val instanceof Object) && immutable(val, opts)) || val;
  },

  set: (((opts || {}).throw === false) && nothrowSet) || throwSet
});

module.exports = immutable;
