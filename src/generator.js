"use strict";

const {map, reduce, forEach} = require("./mapreduce");

const generator = (fn, opts) => {
  const {
    max = Number.POSITIVE_INFINITY
  } = (opts || {});

  const gen = function* () {
    for ( let x = 0; x < max; x++ ) {
      const res = fn();
      if ( res === generator.END )
        break;
      yield res;
    }
  };

  const gi = gen();
  gi.forEach = forEach;
  gi.reduce = reduce;
  gi.map = map;

  return gi;
};

generator.END = Symbol("Generator End");

module.exports = generator;
