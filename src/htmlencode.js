"use strict";

const htmlencode = str => str.replace(/&/g, "&amp;")
  .replace(/>/g, "&gt;")
  .replace(/</g, "&lt;")
  .replace(/"/g, "&quot;");

module.exports = htmlencode;
