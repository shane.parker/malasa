"use strict";

const reduce = function (cb, acc, thisArg) {
  let idx = 0;
  for ( const v of this ) {
    acc = cb.call(thisArg, (acc !== undefined) ? acc : v, v, idx++);
  }
  return acc;
};

const map = function (cb, thisArg) {
  const res = [];
  for ( const v of this ) {
    res.push(cb.call(thisArg, v, res.length));
  }

  return res;
};

const forEach = function (cb, thisArg) {
  let idx = 0;
  for ( const v of this ) {
    cb.call(thisArg, v, idx++);
  }
};

module.exports = {
  map,
  reduce,
  forEach
};
