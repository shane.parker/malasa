"use strict";

const except = e => {throw ((e instanceof Error) ? e : Error(e));};

module.exports = except;
