"use strict";

const event = (...list) => {
  const listeners = new Set([].concat(...list));

  const func = (...params) => listeners.forEach( v => v(...params) );
  func.trigger = (...params) => func(...params);
  func.listen = (l) => listeners.add(l);
  func.ignore = (l) => listeners.delete(l);

  return func;
};

module.exports = event;
