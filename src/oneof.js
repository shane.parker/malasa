
"use strict";

const except = require("./except");

const oneof = (value, options, err) => (options.indexOf(value) >= 0) ? value : except(err);

module.exports = oneof;
