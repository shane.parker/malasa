"use strict";

const sleep = timeout => new Promise( resolve => {
  setTimeout(() => resolve(timeout), timeout);
});

module.exports = sleep;
