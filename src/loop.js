"use strict";

const loop = async (cb) => {
  for ( ;; ) {
    const res = await cb();
    if ( res !== undefined )
       return res;
  }
};

module.exports = loop;
