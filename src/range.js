"use strict";

const {map, reduce, forEach} = require("./mapreduce");

function *range(opts) {
  const {
    start = 0,
    end = Number.POSITIVE_INFINITY,
    step = 1
  } = (opts || {});

  for ( let x = start; x < end; x += step ) {
    yield x;
  }
}

module.exports = (opts) => {
  const gen = range(opts);
  gen.forEach = forEach;
  gen.reduce = reduce;
  gen.map = map;

  return gen;
};
